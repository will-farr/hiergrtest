%%% SETUP

\ifdefined\includeapp
    
\else
    \input{preamble}
    \togglefalse{includeapp}

    \begin{document}
    
    %Title of paper
    \title{Supplemental material for\\``A hierarchical test of general relativity with gravitational waves''}
    
    \input{authors}
    
    \date{\today}
    \maketitle

    % \section{TECHNICAL DETAILS}
\fi

\ifdefined\includeapp
    \newcommand{\FigRealEvents}{Fig.~\ref{fig:RealEvents}}
\else
    \newcommand{\FigRealEvents}{Fig.~3}
\fi

%%% CONTENT BEGINS

\section{Hierarchical inference}

Our goal is to estimate the \emph{posterior} probability density representing the measurement of some parameters of interest, 
conditional on some data or observation.
In order to do this, it is necessary to write down a \emph{prior} probability density function that encodes 
knowledge about the possible values of the parameters before obtaining the data.
We use Bayes' theorem to obtain the posterior from the \emph{likelihood}, which is the probability of the data conditional on the values of the parameters.
For a parameter $x$ and set of data $\vec{d}$, Bayes' theorem is just
\beq
p(x \mid \vec{d}) \propto p(\vec{d} \mid x)\, p(x)\, ,
\eeq
relating the posterior (left) to the likelihood times the prior (right), with a proportionality constant that ensures unitarity.

The specific functional form of the prior is determined according to different expectations for the parameters.
For example, in situations of high ignorance about the expected values, common choices for the prior are a uniform distribution across some broad range 
or a least-informative (aka, ``Jeffrey's'') prior.
Alternatively, the prior may take the form of a multivariate normal distribution with some mean vector and covariance matrix.
The mean informs the analysis of the likely location of the parameter values in the $\nparam$-dimensional parameter space, 
and the covariance sets the scale of the corresponding uncertainty.
Just as with the boundaries of the flat prior, the mean and covariance matrix can be set arbitrarily.

An extension to the above fixed priors is the case where we allow this distribution to vary in some controlled way.
One such example is the case where we wish to marginalize over assumptions about the provenance of a parameter.
Alternatively, as in the main text, besides the values of the parameters for any given observation, we might also be 
interested in the distribution of said parameter in a population of observed data.
Both types of analyses make use of \emph{hierarchical models}, with multiple nested levels of inference with corresponding priors.
Priors on parameters that control distributions of other parameters (e.g. the mean and the covariance of the example priors above) 
are often called \emph{hyperpriors}.

In this paper, we perform two inference levels: the first corresponds to the measurement of ppE-like parameters from individual events, and the second is the measurement of the properties of the population of ppE-like parameters.
Concretely, we begin from the posteriors on $\nparam$ parameters $\deltap_i^{(j)}$ from the data $\data^{(j)}$ for the $\nevent$ events.
The posterior obtained from event $j$ for parameter $i$ takes the form:
\begin{align} \label{eq:single_posterior}
p(\deltap_i^{(j)} \mid \data^{(j)}) &\propto p( \data^{(j)} \mid \deltap_i^{(j)})\, p(\deltap_i^{(j)})\nn \\
&\propto p( \data^{(j)} \mid \deltap_i^{(j)})\, ,
\end{align}
where the last line is only valid under the assumption of a uniform prior on $\deltap_i^{(j)}$, as was applied in the original measurements by LIGO and Virgo \cite{LIGOScientific:2019fpa,GWOSC:O2TGR}.

We then assume that the values for each event are drawn from a normal distribution with mean $\popmu_i$ and standard deviation $\popsig_i$, characteristic of each parameter.
Since the goal is to measure those quantities from the data from \emph{all} events, for each $i$ we want the posterior
\begin{align} \label{eq:population_posterior}
p(\popmu_i,\popsig_i \mid \{\data^{(j)}\}_{j=1}^\nevent) &\propto p(\{\data^{(j)}\}_{j=1}^\nevent \mid \popmu_i,\popsig_i)\, p(\popmu_i,\popsig_i)\nn\\
&\propto p(\popmu_i,\popsig_i) \prod_{j=1}^\nevent p(\data^{(j)}\mid \popmu_i,\popsig_i)\, ,
\end{align}
where we took advantage of the fact that events are statistically independent to factorize the likelihood in the last line.
Each of those factors may be written explicitly in terms of the parameters $\deltap_i^{(j)}$, 
\begin{align} \label{eq:posterior_integral}
p(\data^{(j)}\mid \popmu_i,\popsig_i) = \int &p(\data^{(j)}\mid \deltap_i^{(j)})\times\nn\\
&p(\deltap_i^{(j)}\mid \popmu_i,\popsig_i)\, {\rm d}[\deltap_i^{(j)}]\, ,
\end{align}
obtaining an expression in terms of the individual likelihoods, Eq.~\eqref{eq:single_posterior}.
These likelihoods, $p(\data^{(j)}\mid \deltap_i^{(j)})$---or, rather, the corresponding posteriors with a suitable choice of prior---are what is computed in a regular (nonhierarchical) analysis with the goal of measuring $\deltap_i^{(j)}$,~as is the case in \cite{LIGOScientific:2019fpa}.

The last two equations allow us, then, to measure the population mean and standard deviation for the ppE-like parameters starting from the posterior samples released by the LIGO and Virgo collaborations \cite{GWOSC:O2TGR}, without re-analyzing the raw data (\FigRealEvents{}).
Specifically, we obtain posteriors on the population parameters, Eq.~\eqref{eq:population_posterior}, by sampling over $\popmu_i$ and $\popsig_i$ with a Hamiltonian Monte Carlo algorithm implemented in the \texttt{stan} package~\cite{JSSv076i01}.
To simplify the integration over the $\deltap_i$'s in Eq.~\eqref{eq:posterior_integral}, we internally represent the LIGO-Virgo posteriors via a Gaussian-kernel density fit produced from the samples in \cite{GWOSC:O2TGR}.
%\blue{Our code for doing this is available at \dots}

From a posterior on the hyperparameters, $p(\popmu_i,\popsig_i \mid \{\data^{(j)}\}_{j=1}^\nevent)$, we may also infer the shape of the population distributions themselves.
This can be done by marginalizing over $\popmu_i$ and $\popsig_i$,
\begin{align} \label{eq:inferred_distribution}
p(\deltap_i \mid \{\data^{(j)}\}_{j=1}^\nevent) = \int &p(\deltap_i \mid \popmu_i, \popsig_i)\, \times \nn\\
&p(\popmu_i,\popsig_i \mid \{\data^{(j)}\}_{j=1}^\nevent)\, {\rm d} \popmu_i {\rm d}\popsig_i\, ,
\end{align}
where $p(\ppe_i \mid \popmu_i, \popsig_i) = {\cal N}(\popmu_i, \popsig_i)$ by construction.
In the main text we label the inferred distribution of Eq.~\eqref{eq:inferred_distribution} as ``${\rm d}N/{\rm d}(\deltap_i)$,'' since it can be interpreted as the expected fractional number of events ${\rm d}N$ with a value of the ppE-like coefficient $\deltap_i^{(j)}$ within $[\deltap_i, \deltap_i+{\rm d}(\deltap_i)]$ .


\section{Parametrized tests of GR}

In this study, we consider parametric deformations to the GW signal described by some non-GR quantities 
$\deltap_i$ indexed by $i$, with $\deltap_i = 0$ corresponding to GR.
These new parameters are introduced on top of the 15 usual parameters that describe a GW within GR, with the goal of providing a 
model-independent framework with which to test the theory. The parameters are generally chosen such that they modify a specific aspect 
of the waveform, and hence a non zero value would signal a specific type of violation of GR.
The parametric tests we will consider here are introduced to test the waveform in the different regimes of a binary coalescence: the inspiral, the merger, and the ringdown. 

The inspiral phase is deformed through the post-Einsteinian (ppE) inspiral parameters $\delta\p_i=\ppe_i$~\cite{Yunes:2009ke}.
We define these $\ppe_i$'s as in~\cite{LIGOScientific:2019fpa}, a choice that differs slightly from that of~\cite{Yunes:2009ke} but which is analytically equivalent~\cite{Yunes:2016jcc}.
The usual inspiral phase within GR is usually expressed as an expansion in small velocities,
 referred to as the post-Newtonian (PN) expansion. Each
coefficient in the expansion depends on the system parameters and measurement of the GW phase evolution amounts to measuring said parameters.
The ppE parameters are introduced as relative (or absolute, in some cases where the GR term vanishes) deformations of the normal PN coefficients such that 
$\ppe_i$'s encodes a correction at the $i/2$ PN order. For example, $\ppe_{-2}$ corresponds to a -1PN correction, associated with dipole radiation.

The merger-ringdown and the intermediate regimes are deformed through a different set of post-inspiral parameters, denoted $\delta\hat{\alpha}_i$ and $\delta\hat{\beta}_i$ respectively~\cite{Meidam:2017dgf}.
These parameters encode modifications to the analytic description of those post-inspiral stages as implemented in the \textsc{IMRPhenomPv2} waveform model~\cite{Hannam:2013oca,Khan:2015jqa}.
In particular, the $\delta\hat\alpha_i$'s control the merger-ringdown coefficients $\alpha_i$, which are obtained both from phenomenological fits and black-hole perturbation theory~\cite{Khan:2015jqa}.
The $\delta\hat\beta_i$'s control deviations from the NR-calibrated phenomenological coefficients
$\beta_i$ of the intermediate stage.



%% CONTENT ENDS

\ifdefined\includeapp
\else
    \bibliography{OurRefs}
    \end{document}
\fi
