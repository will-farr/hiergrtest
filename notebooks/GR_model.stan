data {
  int ndet;
  vector[ndet] muobs;
  vector[ndet] sigma_muobs;
}

parameters {
  real mu;  
  real<lower=0> sigma;
  vector[ndet] unit;
}

transformed parameters {
  vector[ndet] mutrue;
  mutrue = mu+sigma*unit;
}

model {
  unit ~ normal(0,1);
  muobs ~ normal(mutrue, sigma_muobs);
}
