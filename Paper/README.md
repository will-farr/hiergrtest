# hiergrtest

Hierarchical methods for testing general relativity.

The content of this repository is licensed under the [CC-BY-4.0 license](https://creativecommons.org/licenses/by/4.0/); you can find a copy of this license in the ``LICENSE.md`` file.

## Instructions

You have the following options to produce different versions of the manuscript PDF:
 * `make` produces a PDF including comments;
 * `make nocomments` produces a clean version of the draft without comments or coloring;
 * `make supplement` produces a version of the manuscript with the appendix excised into a separate supplement;
 * `make arxiv` or `make prl` produce `tar` files for submission to arXiv or journal respectively.

The `make prl` option edits the `preamble.tex` file to make sure that the output will be like in `make supplement`.
