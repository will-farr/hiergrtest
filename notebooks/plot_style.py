import numpy as np
import matplotlib
import matplotlib.pyplot as plt


# ############################################################################
# PLOTTING

# make plots fit the LaTex column size but rescale them for ease of display in the notebook
scale_factor = 4

fig_width_pt = scale_factor*246.0  # Get this from LaTeX using \showthe\columnwidth
inches_per_pt = 1.0/72.27               # Convert pt to inches
fig_ratio = 1 #(np.sqrt(5)-1.0)/2.0         # Plot aspect ratio
fig_width = fig_width_pt*inches_per_pt  # width in inches
fig_height =fig_width*fig_ratio       # height in inches
fig_size_square = [fig_width, fig_height]

fig_ratio = (np.sqrt(5)-1.0)/2.0         # Plot aspect ratio
fig_width = fig_width_pt*inches_per_pt  # width in inches
fig_height =fig_width*fig_ratio       # height in inches
fig_size = [fig_width, fig_height]

# LaTex text font sizse in points (rescaled as above)
fs = scale_factor*10  # general
lfs = 0.9*fs  # legend
tfs0 = 0.8*fs  # ax0 ticks
tfs12 = 0.6*fs # ax1/2 ticks

mplparams = {
    'text.usetex': True,  # use LaTeX for all text
    'axes.linewidth': 1,  # set axes linewidths to 0.5
    'axes.grid': False,  # add a grid
    'axes.labelweight': 'normal',
    'font.size': fs,
    'font.serif': 'Computer Modern Roman',
    'xtick.labelsize': fs,
    'ytick.labelsize': fs,
}
matplotlib.rcParams.update(mplparams)
matplotlib.rc('text.latex')

import seaborn as sns
# sns.set(font_scale=1.5)
sns.axes_style("white")
sns.set_style("ticks", {'font.family': 'serif'})

