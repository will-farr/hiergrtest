Simple code and demonstration of fitting for the mean and scatter of a
population of observations; I focus on the use case of testing GR using GW
observations by aggregating posterior samples over events and fitting a Gaussian
population.

PDFs are automatically generated when the content changes. The following link is updated automatically:
 * [Latest PDF](https://git.ligo.org/max-isi//hiergrtest/-/jobs/artifacts/master/file/Paper/paper.pdf?job=compile_pdf)
